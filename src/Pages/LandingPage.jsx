import React, { useEffect, useState } from "react";
import List from "../Components/List";
import Navbar from "../Components/Navbar";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { TextField } from "@mui/material";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    display: "flex",
    flexDirection: "column",
};

const user = [
    // {
    //     id: 1,
    //     name: "saya",
    //     hobby: "makan",
    //     address: "sii"
    // }

]


let searchResult = []

const LandingPage = () => {

    const [id, setId] = useState(0)
    const [name, setName] = useState("")
    const [address, setAddress] = useState("")
    const [hobby, setHobby] = useState("")
    const [open, setOpen] = useState(false);
    const [filter, setFilter] = useState("")

    const handleSearch = (e) => {
        setFilter(e.target.value)
        const arr = []
        const dat = user.filter(usr => {
            return usr.name.toLowerCase().includes(filter.toLowerCase());
        });
        if (dat.length > 0) {
            searchResult = dat
        } else if (filter.length <= 0) {
            searchResult = arr
        }
    }

    const handleOpen = () => {
        setOpen(true)
    };
    const handleClose = () => {
        setOpen(false)
        setName("")
        setAddress("")
        setHobby("")
        setId("")
    };

    const handleEdit = (nama, alamat, hobi, id) => {
        handleOpen()
        setId(id)
        setName(nama)
        setAddress(alamat)
        setHobby(hobi)
    }

    const findData = (id) => user.findIndex((obj => obj.id == id))

    const handleSave = () => {
        if (name === "" || address === "" || hobby === "") {
            alert("please fill all the forms")
        } else if (findData(id) >= 0) {
            const objIndex = findData(id)
            user[objIndex].name = name
            user[objIndex].hobby = hobby
            user[objIndex].address = address
            handleClose()
        } else {
            const id = user.length + 1
            user.push({ id, name, address, hobby })
            setName("")
            setAddress("")
            setHobby("")
            handleClose()
        }
    }


    useEffect(() => {
    }, [user, filter]);

    return (
        <>
            <Navbar func={handleOpen} user={user} />
            <TextField label="Search" sx={{ marginTop: '1rem' }} onChange={(e) => handleSearch(e)} />

            {searchResult.length > 0 && filter.length > 0 ? <List user={searchResult} func={handleEdit} /> : <List user={user} func={handleEdit} />}
            <div>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography textAlign="center" id="modal-modal-title" variant="h6" component="h2">
                            Add User
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            Name
                        </Typography>
                        <TextField onChange={(e) => setName(e.target.value)} value={name} />
                        <Typography id="modal-modal-description" sx={{ mt: 2 }} >
                            Address
                        </Typography>
                        <TextField onChange={(e) => setAddress(e.target.value)} value={address} />
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            Hobby
                        </Typography>
                        <TextField onChange={(e) => setHobby(e.target.value)} value={hobby} />
                        <Box sx={{ display: "flex", justifyContent: "center" }}>
                            <Button sx={{ backgroundColor: "#00c2cb", color: "black", width: "30%", marginTop: "2rem" }} onClick={handleSave}>Save</Button>
                        </Box>
                    </Box>
                </Modal>
            </div>
        </>
    );
};

export default LandingPage;
