import React from "react";
import { useLocation } from "react-router-dom";
import UserList from "../Components/UserList";

const UserView = () => {
    const data = useLocation()
    const user = data?.state?.data
    return (
        <div>
            <UserList data={user} />
        </div>
    );
};

export default UserView;
