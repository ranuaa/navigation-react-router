import { Container, Typography } from "@mui/material";
import React from "react";
import Navbar from "../Components/Navbar";

const About = () => {
    return (
        <>
            <Navbar />
            <Container>
                <Typography variant='h5'>
                    About This App
                </Typography>
                <Typography variant='p'>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, eveniet aperiam quos at similique sit placeat inventore, quasi beatae nobis itaque temporibus quaerat repudiandae illum cupiditate minima quibusdam cumque in.
                </Typography>
                <br />
                <Typography variant='p'>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, eveniet aperiam quos at similique sit placeat inventore, quasi beatae nobis itaque temporibus quaerat repudiandae illum cupiditate minima quibusdam cumque in.
                </Typography>
            </Container>
        </>
    );
};

export default About;
