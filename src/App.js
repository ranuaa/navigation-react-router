import React from "react";
import Navbar from "./Components/Navbar";
import MainRoutes from "./Routing/MainRoutes";

function App() {
  return (
    <>
    <MainRoutes/>
    </>
  );
}

export default App;
