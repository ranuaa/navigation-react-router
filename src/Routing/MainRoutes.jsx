import React from "react";
import { Route, Routes } from "react-router-dom";
import About from "../Pages/About";
import LandingPage from "../Pages/LandingPage";
import UserView from "../Pages/UserView";

const MainRoutes = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<LandingPage />} />
                <Route path="/view/:id" element={<UserView />} />
                <Route path="/hobby" element={<UserView page="hobby" />} />
                <Route path="/about" element={<About />} />
            </Routes>
        </>
    );
};

export default MainRoutes;
