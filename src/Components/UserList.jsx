import ImageIcon from '@mui/icons-material/Image';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import React from "react";
import { useLocation } from 'react-router-dom';
import Navbar from "./Navbar";

const UserList = ({ data, page }) => {
    const Location = useLocation()
    const hobbies = Location.state.data
    console.log(hobbies)
    return (
        <>
            <Navbar />
            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                {Location?.state.page ?
                    hobbies?.map((hobby) => {
                        return (
                            <ListItem>
                                {console.log(hobby)}
                                <ListItemText primary={hobby?.hobby} />
                            </ListItem>
                        )
                    })
                    // <>
                    //     {console.log(hobbies)}
                    // </>
                    :
                    <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                                <ImageIcon />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={data?.name} secondary={`Address : ${data?.address} || Hobby : ${data?.hobby}`} />
                    </ListItem>
                }
            </List>
        </>
    );
};

export default UserList;
