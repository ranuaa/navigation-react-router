import React from "react";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { NavLink, useNavigate } from "react-router-dom";

const Navbar = ({ func, user }) => {
    const navigate = useNavigate()
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Box sx={{ flexGrow: 1, display: 'flex', gap: '20px' }}>
                        <Typography variant="h6" component="div" onClick={() => navigate('/')}>
                            MyApp
                        </Typography>
                        <NavLink to='/about'>
                            <Typography variant="h6" component="div" >
                                About
                            </Typography>
                        </NavLink>
                        <NavLink to='/hobby' state={{ data: user, page: "hobby" }}>
                            <Typography variant="h6" component="div" >
                                Hobby
                            </Typography>
                        </NavLink>
                    </Box>
                    <Button sx={{ backgroundColor: "#00c2cb", color: "white", borderRadius: "25px" }} onClick={func}>Add User</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default Navbar;
